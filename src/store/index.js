import { createStore } from 'vuex'
import axios from 'axios';

export default createStore({
  state: {
    dragons: [],
    rockets: []
  },
  getters: {
  },
  mutations: {
    loadDragons(state, dragons) {
      state.dragons = dragons;
    },
    loadRockets(state, rockets) {
      state.rockets = rockets;
    },
  },
  actions: {
    loadDragons({commit}) {
      axios.get('https://api.spacexdata.com/v4/dragons')
      .then(response => {
        commit('loadDragons', response.data);
        console.log(response.data);
      });
    },
    loadRockets({commit}) {
      
      axios.get('https://api.spacexdata.com/v4/rockets')
      .then(response => {
        commit('loadRockets', response.data);
        console.log(response.data);
      });
    },
  },
  modules: {
  }
})

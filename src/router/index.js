import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/capsule',
    name: 'capsule',
    // route level code-splitting
    // this generates a separate chunk (capsule.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "capsule" */ '../views/CapsuleView.vue')
  },
  {
    path: '/rocket/:id',
    name: 'rocket',
    // route level code-splitting
    // this generates a separate chunk (rocket.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "rocket" */ '../views/RocketView.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
